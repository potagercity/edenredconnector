# Omnipay: Edenred

**Edenred driver for the Omnipay PHP payment processing library**

## Installation

Omnipay is installed via [Composer](http://getcomposer.org/). To install, simply require `league/omnipay` and `potagercity/edenred` with Composer:

```
composer require league/omnipay omnipay/stripe
```


## Docker

Composer can be run with

```
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install
```


