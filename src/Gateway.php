<?php

namespace Potagercity\Edenred;

use Omnipay\Common\AbstractGateway;
use Omnipay\Common\Http\ClientInterface;
use Omnipay\Common\Message\RequestInterface;
use Potagercity\Edenred\Message\AuthorizeRequest;
use Potagercity\Edenred\Message\CancelRequest;
use Potagercity\Edenred\Message\CaptureRequest;
use Potagercity\Edenred\Message\EstimateChargesRequest;
use Potagercity\Edenred\Message\PurchaseRequest;
use Potagercity\Edenred\Message\RefundRequest;
use Symfony\Component\HttpFoundation\Request as HttpRequest;


class Gateway extends AbstractGateway
{

    protected $mid;

    protected $clientId;

    protected $clientSecret;

    protected $serverUrl;


    public function __construct(ClientInterface $httpClient = null, HttpRequest $httpRequest = null)
    {
        parent::__construct($httpClient, $httpRequest);
    }

    public function setMid($mid)
    {
        $this->mid = $mid;
    }

    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    public function setServerUrl($serverUrl)
    {
        $this->serverUrl = $serverUrl;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'edenred';
    }

    protected function getGlobalParameters()
    {
        return [
            'mid'          => $this->mid,
            'clientId'     => $this->clientId,
            'clientSecret' => $this->clientSecret,
            'serverUrl'    => $this->serverUrl,

        ];
    }

    public function authorize(array $options = array()) : RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(AuthorizeRequest::class, $params);
    }


    public function capture(array $options = array()) : RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(CaptureRequest::class, $params);
    }


    public function purchase(array $options = array()) : RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(PurchaseRequest::class, $params);
    }


    public function refund(array $options = array()) : RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(RefundRequest::class, $params);
    }

    public function cancel(array $parameters = array())
    {
        $params = array_merge($parameters, $this->getGlobalParameters());

        return $this->createRequest(CancelRequest::class, $params);
    }

    public function estimateCharges(array $parameters = array())
    {

        $params = array_merge($parameters, $this->getGlobalParameters());

        return $this->createRequest(EstimateChargesRequest::class, $params);
    }
}
