<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Edenred\Message;


class AuthorizeRequest extends AbstractRequest
{

    public function getData()
    {
        $this->validate('mid');
        $this->validate('clientId');
        $this->validate('clientSecret');
        $this->validate('accessToken');

        $this->validate('amount');

        $data = array();

        if ($this->getMid()) {
            $data['mid'] = $this->getMid();
        }

        if ($this->getAmount()) {
            /**
             * EdenredApi speak in cents
             */

            $amount = $this->amountToCents($this->getAmount());
            $data['amount'] = $amount;
        }

        if($this->getOrderRef()){
            $data['order_ref'] = $this->getOrderRef();
        }

        $data['capture_mode'] = "manual";


        return $data;
    }

    /**
     * @param string $value
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setMid($value)
    {
        return $this->setParameter('mid', $value);
    }

    /**
     * @return mixed
     */
    public function getMid()
    {
        return $this->getParameter('mid');
    }

    /**
     * @param string $value
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setAmount($value)
    {
        return $this->setParameter('amount', $value);
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return ($this->getParameter('amount'));
    }

    /**
     * @param string $value
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setOrderRef($value)
    {
        /**
         * EdenredApi speak in cents
         */
        return $this->setParameter('orderRef', $value);
    }

    /**
     * @return mixed
     */
    public function getOrderRef()
    {
        return ($this->getParameter('orderRef'));
    }


    public function getEndpoint()
    {
        return $this->endpoint . '/transactions';
    }

    /**
     * @inheritdoc
     */
    protected function createResponse($data, $headers = [])
    {
        return $this->response = new Response($this, $data, $headers);
    }

    public function getHttpMethod()
    {
        return 'POST';
    }
}
