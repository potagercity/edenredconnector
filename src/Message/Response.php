<?php

namespace Potagercity\Edenred\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RedirectResponseInterface;
use Omnipay\Common\Message\RequestInterface;

class Response extends AbstractResponse implements RedirectResponseInterface
{
    /**
     * Request id
     *
     * @var string URL
     */
    protected $requestId = null;

    /**
     * @var array
     */
    protected $headers = [];

    public function __construct(RequestInterface $request, $data, $headers = [])
    {
        parent::__construct($request, json_decode($data, true));
        $this->headers = $headers;
    }

    public function isSuccessful()
    {
        try {
            $data = $this->getData();
            if (isset($data['meta'])) {
                return $data['meta']['status'] == 'succeeded';
            }
        } catch (\Throwable $t) {
            return false;
        }

        return false;
    }


}
