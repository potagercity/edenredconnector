<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Edenred\Message;


class RefundRequest extends AbstractRequest
{
    public function getData()
    {
        $this->validate('transactionReference');
        $this->validate('amount');

        $data = [];

        $amount = $this->amountToCents($this->getAmount());
        $data['amount'] = $amount;

        /**
         * CF mail de Julien Bonin
         *
         * Il faut que tu ajoutes le capture_mode=‘manual’ dans le /capture et ça sera bon !
         * En résumé, il faut ajouter ce « capture_mode=‘manual’ sur l’autorise, la capture et le refund
         * Julien
         */
        $data['capture_mode'] = "manual";

        return $data;
    }

    public function getEndpoint()
    {
        return $this->endpoint . '/transactions/' . $this->getTransactionReference() . '/actions/refund';
    }

    public function getHttpMethod()
    {
        return 'POST';
    }
}
