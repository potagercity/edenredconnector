<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Edenred\Message;


class CaptureRequest extends AbstractRequest
{
    public function getData()
    {
        $this->validate('transactionReference');

        $this->validate('clientId');
        $this->validate('clientSecret');
        $this->validate('accessToken');
        $this->validate('amount');


        $data = [];

        $amount = $this->amountToCents($this->getAmount());

        $data['amount'] = $amount;

        if ($this->getOrderRef()) {
            $data['order_ref'] = $this->getOrderRef();
        }

        /**
         * CF mail de Julien Bonin
         *
         * Il faut que tu ajoutes le capture_mode=‘manual’ dans le /capture et ça sera bon !
         * En résumé, il faut ajouter ce « capture_mode=‘manual’ sur l’autorise, la capture et le refund
         * Julien
         */
        $data['capture_mode'] = "manual";

        return $data;
    }

    public function getEndpoint()
    {
        return $this->endpoint . '/transactions/' . $this->getTransactionReference() . '/actions/capture';
    }

    public function getHttpMethod()
    {
        return 'POST';
    }

    /**
     * @param string $value
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setOrderRef($value)
    {
        /**
         * EdenredApi speak in cents
         */
        return $this->setParameter('orderRef', $value);
    }

    /**
     * @return mixed
     */
    public function getOrderRef()
    {
        return ($this->getParameter('orderRef'));
    }
}
